package cn.afterturn.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 私有日志处理,自动加载实体类
 * 
 * @author JueYue
 * @date 2014年10月14日 下午2:04:41
 */
public final class LemurLogger {

    private LemurLogger() {

    }

    /**
     * 获取slf4j 日志对象 默认加载调用对象
     * 
     * @return
     */
    public static final Logger getLogger() {
        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        return LoggerFactory.getLogger(sts[2].getClassName());
    }

    /**
     * 获取slf4j 日志对象
     * 
     * @param clazz
     *            所属class
     * @return
     */
    public static final Logger getLogger(Class<?> clazz) {
        return LoggerFactory.getLogger(clazz);
    }

    /**
     * 获取slf4j 日志对象
     * 
     * @param classname
     *            所属class
     * @return
     */
    public static final Logger getLogger(String classname) {
        try {
            return LoggerFactory.getLogger(Class.forName(classname));
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

}
