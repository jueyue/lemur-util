package cn.afterturn.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 解析字符串
 * @author JueYue
 * @date 2014年11月30日 下午9:18:13
 */
public final class ParseCode {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseCode.class);

    public static final String UTF8   = "utf-8";
    public static final String GBK    = "gbk";
    public static final String GB2312 = "gb2312";
    public static final String ISO    = "ISO-8859-1";

    public static final String decode(String obj) {
        try {
            return URLDecoder.decode(obj, "UTF8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("解析字符串失败:{}", obj);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static final String decode(String obj, String type) {
        try {
            return URLDecoder.decode(obj, type);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("解析字符串失败:{}", obj);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static final String encode(String obj) {
        try {
            return URLEncoder.encode(obj, "UTF8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("解析字符串失败:{}", obj);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static final String encode(String obj, String type) {
        try {
            return URLEncoder.encode(obj, type);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("解析字符串失败:{}", obj);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

}
