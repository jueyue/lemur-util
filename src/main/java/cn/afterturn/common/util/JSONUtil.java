package cn.afterturn.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Json工具类
 * 
 * @author JueYue
 * @date 2014年2月24日 下午7:25:57
 */
@SuppressWarnings("unchecked")
public final class JSONUtil {

    private JSONUtil() {

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JSONUtil.class);

    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        //解析器支持解析单引号
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        //解析器支持解析结束符
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private static JavaType getCollectionType(Class<?> collectionClass,
                                              Class<?>... elementClasses) {
        return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    public static final <T> T parseJson(String json, Class<?> clss) {
        try {
            return (T) mapper.readValue(json, clss);
        } catch (JsonProcessingException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static final <T> List<T> parseJsonList(String json, Class<?> clss) {
        JavaType javaType = getCollectionType(ArrayList.class, clss);
        try {
            return mapper.readValue(json, javaType);
        } catch (JsonParseException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error(e.getMessage(), e);
        } catch (JsonMappingException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error("解析JSON串失败:{}", json);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static final String toJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOGGER.error("JSON序列化失败:{}", obj);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

}
